#!/bin/bash

COMPOSEBIN=${COMPOSEBIN-"docker-compose"}

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

VOLUMES="-v /media/secondary/plex/config:/config -v /media/secondary/plex/data/tvshows:/data/tvshows -v /media/secondary/plex/data/movies:/data/movies -v /media/secondary/plex/transcode:/transcode"

function performStart
{
  docker run --name=plex --rm -e VERSION=latest -e PUID=500 -e PGID=500 -e TZ="Europe/Paris" --net=host $VOLUMES linuxserver/plex
}

function performStop
{
  docker kill plex
}

function showOpsHelp
{
  cat <<EOF
Usage: ops package outputfile
Usage: ops restore
EOF
}

function showHelp
{
  cat <<EOF
Usage: start
Usage: stop
EOF
  showOpsHelp
}

function performOps
{
  case "$1" in
    package)
      [ -z "$2" ] && echo "Missing output file argument" && exit 1;
      [ -e "$2" ] && [ ! -w "$2" ] && echo "Target file is not writeable" && exit 1;

      performStop
      docker run --rm --name=plex_ops -v $(pwd):/var/ops $VOLUMES fredericvauchelles/docker-plex3-ops:latest package /var/ops/$2
      performStart
      ;;
    restore)
      [ -z "$2" ] && echo "Missing output file argument" && exit 1;
      [ -e "$2" ] && [ ! -w "$2" ] && echo "Target file is not writeable" && exit 1;
      performStop
      docker run --rm --name=plex_ops -v $(pwd):/var/ops $VOLUMES fredericvauchelles/docker-plex3-ops:latest restore /var/ops/$2
      performStart
      ;;
    *)
      showOpsHelp
      ;;
  esac
}

function main
{
  case "$1" in
    start)
      performStart
      ;;
    stop)
      performStop
      ;;
    ops)
      shift
      performOps "$@"
      ;;
    *)
      showHelp
      ;;
  esac
}

main "$@"